{ config, home, lib, pkgs, ... }:
let
  attic = "${pkgs.coreutils}/bin/nice ${pkgs.attic}/bin/attic";
  repository = "${home}/repository.attic";
  excludes = lib.concatMapStringsSep " " (p:
    "--exclude ${p}"
  ) config.excludes;
  creates = lib.concatMapStrings (p: ''
    ${attic} create --stats ${excludes} \
      "${repository}::${p}-$(${pkgs.coreutils}/bin/date +%Y%m%d-%s)" ${p}
  '') config.paths;
in {
  options = {
    excludes = lib.mkOption {
      type = lib.types.listOf lib.types.str;
      default = [repository];
    };
    paths = lib.mkOption {
      type = lib.types.listOf lib.types.str;
    };
  };
  run = pkgs.writeBash (baseNameOf ./.) ''
    ${attic} check ${repository} || ${attic} init ${repository}
    ${creates}
    ${attic} prune \
      --keep-hourly=10 \
      --keep-daily=10 \
      --keep-weekly=10 \
      --verbose ${repository}
    exec ${pkgs.coreutils}/bin/sleep $(( 60 * 60 ))
  '';
}
